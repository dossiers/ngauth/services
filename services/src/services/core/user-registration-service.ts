import { Observable } from 'rxjs/Observable';

// import { UserRegistrationInfo, CognitoResult, NewPasswordInfo } from '@ngauth/core';
import { UserRegistrationInfo, CognitoResult } from '@ngauth/core';



export interface IUserRegistrationService {
  register(user: UserRegistrationInfo): Observable<CognitoResult>;
  confirmRegistration(username: string, confirmationCode: string): Observable<CognitoResult>;
  resendCode(username: string): Observable<CognitoResult>;
  // changePassword(newPasswordInfo: NewPasswordInfo): Observable<CognitoResult>;
}
