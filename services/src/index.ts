import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgCoreCoreModule } from '@ngcore/core';
import { NgAuthCoreModule } from '@ngauth/core';

import { AuthConfigManager } from './services/config/auth-config-manager';
// import { IUserAttributesService } from './services/core/user-attributes-service';
// import { IUserLoginService } from './services/core/user-login-service';
// import { IUserRegistrationService } from './services/core/user-registration-service';
import { UserAuthServiceFactory } from './services/factory/user-auth-service-factory';

export * from './services/config/auth-config-manager';
export * from './services/core/user-attributes-service';
export * from './services/core/user-login-service';
export * from './services/core/user-registration-service';
export * from './services/factory/user-auth-service-factory';


@NgModule({
  imports: [
    CommonModule,
    NgCoreCoreModule.forRoot(),
    NgAuthCoreModule.forRoot(),
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class NgAuthServicesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NgAuthServicesModule,
      providers: [
        AuthConfigManager,
        UserAuthServiceFactory,
        // { provide: UserAuthServiceFactory, useClass: CognitoUserAuthServiceFactory }
      ]
    };
  }
}
